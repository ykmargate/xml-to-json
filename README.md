### Integration Engineer Coding Challenge

#### Challenge

API: https://www.senate.gov/general/contact_information/senators_cfm.xml

Using the following API, choose any programming language that you prefer and perform the following tasks:
1. **Retrieve the contents of the contact list from the API in the raw XML format.**
2.	**For the data contained inside the _Member_ fields in the XML file, convert the data into the JSON format listed below.**
      ```json
      {
      "firstName": "First", 
      "lastName": "Last", 
      "fullName": "First Last",
      "chartId": "Contents of bioguide_id:", 
      "mobile": "Phone",
      "address": {
          "street": "123 Main Street", 
          "city": "Orlando",
          "state": "FL", 
          "postal": 32825
      }
      }
      ```
3.	**Print the converted data**        
      a. This should be completed once all of the data has been converted.  
      b. Use the standard functionality for the language to accomplish this. I.e. console.log() for Javascript etc...

Live demo http://mend.floridawebdev.com/xmltojson.php
