<?php
$xmlString = file_get_contents('https://www.senate.gov/general/contact_information/senators_cfm.xml');
$xml = new SimpleXMLElement($xmlString);
$output = [];
foreach ($xml->member as $member){
  $fullAddress = explode(' ', (string)$member->address);
  $postal = array_pop($fullAddress);
  $state = array_pop($fullAddress);
  $city = array_pop($fullAddress);
  $address = implode(' ', $fullAddress);
  $output[] = [
      "member" => [
          "firstName"  => (string)$member->first_name,
          "lastName"   => (string)$member->last_name,
          "fullName"   => (string)$member->first_name . ' ' . $member->last_name,
          "chartId"    => (string)$member->bioguide_id,
          "mobile"     => (string)$member->phone,
          "address"    => [
              "street" => $address,
              "city"   => $city,
              "state"  => $state,
              "postal" => $postal
          ]
      ]
  ];
}
//print "<pre>";
//print_r($output);
//print "</pre>";
$json = json_encode(["members" => $output]);
header('Content-Type: application/json');
print $json;



